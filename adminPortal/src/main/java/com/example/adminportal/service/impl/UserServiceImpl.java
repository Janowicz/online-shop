package com.example.adminportal.service.impl;

import com.example.adminportal.domain.User;
import com.example.adminportal.domain.security.UserRole;
import com.example.adminportal.repository.RoleRepository;
import com.example.adminportal.repository.UserRepository;
import com.example.adminportal.service.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Set;

@Service
public class UserServiceImpl implements UserService {

    private static final Logger LOG = LoggerFactory.getLogger(UserService.class);

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private RoleRepository roleRepository;

    @Override
    public User createUser(User user, Set<UserRole> userRoles) {

        User localeUser = userRepository.findByUsername(user.getUsername());

        if (localeUser != null){
            LOG.info("Użytkownik {} już istnieje!", user.getUsername());
        } else {
            for (UserRole userRole :userRoles){
                roleRepository.save(userRole.getRole());
            }

            user.getUserRoles().addAll(userRoles);

            localeUser = userRepository.save(user);
        }

        return localeUser;
    }

    @Override
    public User save(User user) {

        return userRepository.save(user);
    }
}
