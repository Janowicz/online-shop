package com.example.adminportal.service;

import com.example.adminportal.domain.Picture;

import java.util.List;
import java.util.Optional;

public interface PictureService {

    Picture save(Picture picture);

    List<Picture> findAll();

    Optional<Picture> findById(Long id);

    void removeById(Long id);
}
