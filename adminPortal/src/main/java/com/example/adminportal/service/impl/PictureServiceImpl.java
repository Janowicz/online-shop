package com.example.adminportal.service.impl;

import com.example.adminportal.domain.Picture;
import com.example.adminportal.repository.PictureRepository;
import com.example.adminportal.service.PictureService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class PictureServiceImpl implements PictureService {

    @Autowired
    private PictureRepository pictureRepository;

    @Override
    public Picture save(Picture picture) {
        return pictureRepository.save(picture);
    }

    @Override
    public List<Picture> findAll() {
        return (List<Picture>) pictureRepository.findAll();
    }

    @Override
    public Optional<Picture> findById(Long id) {
        return pictureRepository.findById(id);
    }

    @Override
    public void removeById(Long id) {
        pictureRepository.deleteById(id);
    }


}
