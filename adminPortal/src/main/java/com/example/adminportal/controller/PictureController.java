package com.example.adminportal.controller;

import com.example.adminportal.domain.Picture;
import com.example.adminportal.service.PictureService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.Optional;

@Controller
@RequestMapping("/picture")
public class PictureController {

    private final String imagePath = "C:\\Users\\mjano\\Desktop\\JavaProjects\\shop\\adminPortal\\src\\main\\resources\\static\\image\\picture\\";

    @Autowired
    PictureService pictureService;

    @RequestMapping(value = "/add", method = RequestMethod.GET)
    public String addPicture(Model model) {

        Picture picture = new Picture();
        model.addAttribute("picture", picture);

        return "addPicture";
    }

    @RequestMapping(value = "/add", method = RequestMethod.POST)
    public String addPicturePost(
            @ModelAttribute("picture") Picture picture,
            HttpServletRequest request) {

        pictureService.save(picture);

        MultipartFile pictureImage = picture.getPictureImage();

        try {
            byte[] bytes = pictureImage.getBytes();
            String pictureName = picture.getId() + ".png";
            BufferedOutputStream stream = new BufferedOutputStream(
                    new FileOutputStream(
                            new File(imagePath + pictureName)));
            stream.write(bytes);
            stream.flush();
            stream.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return "redirect:pictureList";
    }

    @RequestMapping("/pictureInfo")
    public String pictureInfo(@RequestParam("id") Long id, Model model){

        Optional<Picture> picture = pictureService.findById(id);
        model.addAttribute("picture", picture.get());

        return "pictureInfo";
    }

    @RequestMapping("/updatePicture")
    public String updatePicture(@RequestParam("id") Long id, Model model){

        Optional<Picture> picture = pictureService.findById(id);
        model.addAttribute("picture", picture.get());

        return "updatePicture";
    }

    @RequestMapping(value = "/updatePicture", method = RequestMethod.POST)
    public String updatePicturePost(@ModelAttribute("picture") Picture picture, HttpServletRequest request){



        MultipartFile pictureImage = picture.getPictureImage();

        if (!pictureImage.isEmpty()){
            try {
                byte[] bytes = pictureImage.getBytes();
                String pictureName = picture.getId() + ".png";

                Files.delete(Paths.get(imagePath + pictureName));

                BufferedOutputStream stream = new BufferedOutputStream(
                        new FileOutputStream(
                                new File(imagePath + pictureName)));
                stream.write(bytes);
                stream.flush();
                stream.close();
                pictureService.save(picture);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return "redirect:/picture/pictureInfo?id=" + picture.getId();
    }

    @RequestMapping("/pictureList")
    public String pictureList(Model model) {

        List<Picture> listOfPictures = pictureService.findAll();
        model.addAttribute("listOfPictures", listOfPictures);

        return "pictureList";
    }

    @RequestMapping("/removePicture")
    public String removePicture(
            @ModelAttribute("id") Long id,
            Model model) {

//        pictureService.removeById(Long.parseLong(id.substring(11)));
        pictureService.removeById(id);
        List<Picture> listOfPictures = pictureService.findAll();
        model.addAttribute("listOfPictures", listOfPictures);

        return "pictureList";
//        return "redirect:/picture/pictureList";
    }
}
