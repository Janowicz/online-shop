package com.example.adminportal.controller;

import com.example.adminportal.service.PictureService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;

@RestController
public class ResourceController {

    @Autowired
    private PictureService pictureService;

    @RequestMapping(value = "/picture/removeList", method = RequestMethod.POST)
    public String removeList(
            @RequestBody ArrayList<String> pictureIdList,
            Model model){

        for (String id : pictureIdList) {
            String pictureId = id.substring(11);
            pictureService.removeById(Long.parseLong(pictureId));
        }

        return "Usunięto pomyślnie.";
    }

}
