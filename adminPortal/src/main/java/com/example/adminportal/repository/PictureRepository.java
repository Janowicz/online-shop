package com.example.adminportal.repository;

import com.example.adminportal.domain.Picture;
import org.springframework.data.repository.CrudRepository;

public interface PictureRepository extends CrudRepository<Picture, Long> {

}
