/**
 *
 */

jQuery.noConflict()(function ($) {
    $(document).ready(function () {
        $('.delete-picture').on('click', function () {
            /*<![CDATA[*/
            var path = /*[[@{/}]]*/ 'remove';
            /*]]>*/

            var id = $(this).attr('id');

            bootbox.confirm({
                message: "Czy na pewno chcesz usunąć ten obraz?",
                buttons: {
                    confirm: {
                        label: 'Tak'
                    },
                    cancel: {
                        label: 'Nie'
                    }
                },
                callback: function (confirmed) {
                    if (confirmed) {
                        $.post(path, {'id': id}, function (res) {
                            location.reload();
                        });
                    }
                }
            });
        });


        var pictureIdList = [];

        $('.checkboxPicture').click(function () {
            var id = $(this).attr('id');
            if (this.checked) {
                pictureIdList.push(id);
            } else {
                pictureIdList.splice(pictureIdList.indexOf(id), 1);
            }
        })

        $('#deleteSelected').click(function () {
            /*<![CDATA[*/
            var path = /*[[@{/}]]*/ 'removeList';
            /*]]>*/

            bootbox.confirm({
                message: "Czy na pewno chcesz usunąć wszystkie zaznaczone obrazy?",
                buttons: {
                    confirm: {
                        label: 'Tak'
                    },
                    cancel: {
                        label: 'Nie'
                    }
                },
                callback: function (confirmed) {
                    if (confirmed) {
                        $.ajax({
                            type: 'POST',
                            url: path,
                            data: JSON.stringify(pictureIdList),
                            contentType: "application/json",
                            success: function (res) {
                                console.log(res);
                                location.reload();
                            },
                            error: function (res) {
                                console.log(res);
                                location.reload();
                            }
                        });
                    }
                }
            });
        });

        $("#selectAllPictures").click(function () {
            if ($(this).prop("checked") === true) {
                $(".checkboxPicture").prop("checked", true);
            } else if ($(this).prop("checked") === false) {
                $(".checkboxPicture").prop("checked", false);
            }
        })
    });
});