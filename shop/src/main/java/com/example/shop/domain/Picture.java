package com.example.shop.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.springframework.web.multipart.MultipartFile;

import javax.persistence.*;
import java.util.List;

@Entity
public class Picture {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private String title;
    private String size;
    private String category;
    private String weight;
    private double listPrice;
    private double ourPrice;
    private int inStockNumber;
    private boolean active = true;

    @Column(columnDefinition = "text")
    private String description;

    @Transient
    private MultipartFile pictureImage;

    @OneToMany
    @JsonIgnore
    private List<PictureToCartItem> pictureToCartItemList;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getSize() {
        return size;
    }

    public void setSize(String size) {
        this.size = size;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getWeight() {
        return weight;
    }

    public void setWeight(String weight) {
        this.weight = weight;
    }

    public double getListPrice() {
        return listPrice;
    }

    public void setListPrice(double listPrice) {
        this.listPrice = listPrice;
    }

    public double getOurPrice() {
        return ourPrice;
    }

    public void setOurPrice(double ourPrice) {
        this.ourPrice = ourPrice;
    }

    public int getInStockNumber() {
        return inStockNumber;
    }

    public void setInStockNumber(int inStockNumber) {
        this.inStockNumber = inStockNumber;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public MultipartFile getPictureImage() {
        return pictureImage;
    }

    public void setPictureImage(MultipartFile pictureImage) {
        this.pictureImage = pictureImage;
    }

    public List<PictureToCartItem> getPictureToCartItemList() {
        return pictureToCartItemList;
    }

    public void setPictureToCartItemList(List<PictureToCartItem> pictureToCartItemList) {
        this.pictureToCartItemList = pictureToCartItemList;
    }
}
