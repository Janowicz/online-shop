package com.example.shop.utility;

import com.example.shop.domain.Order;
import com.example.shop.domain.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.mail.javamail.MimeMessagePreparator;
import org.springframework.stereotype.Component;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;

import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.util.Locale;

@Component
public class MailConstructor {

    @Autowired
    private Environment environment;

    @Autowired
    private TemplateEngine templateEngine;

    public SimpleMailMessage constructResetTokenEmail(
            String contextPath, Locale locale, String token, User user, String password){

        String url = contextPath + "/newUser?token=" + token;
        String message = "\nKliknij w link, aby zweryfikować Twój e-mail i edytować swoje informacje osobowe." +
                " Twoje hasło: \n" + password;
        SimpleMailMessage simpleMailMessage = new SimpleMailMessage();
        simpleMailMessage.setTo(user.getEmail());
        simpleMailMessage.setSubject("String Very Art - nowe konto");
        simpleMailMessage.setText(url + message);
        simpleMailMessage.setFrom(environment.getProperty("support.email"));

        return simpleMailMessage;
    }

    public MimeMessagePreparator constructOrderConfirmationEmail(User user, Order order, Locale locale){

        Context context = new Context();
        context.setVariable("order", order);
        context.setVariable("user", user);
        context.setVariable("cartItemList", order.getCartItemList());

        String text = templateEngine.process("orderConfirmationEmailTemplate", context);

        MimeMessagePreparator messagePreparator = new MimeMessagePreparator() {

            @Override
            public void prepare(MimeMessage mimeMessage) throws Exception {

                MimeMessageHelper email = new MimeMessageHelper(mimeMessage);
                email.setTo(user.getEmail());
                email.setSubject("Potwierdzenie zamówienia - " + order.getId());
                email.setText(text, true);
                email.setFrom(new InternetAddress("m.janowicz1993@wp.pl"));
            }
        };

        return messagePreparator;
    }

}
