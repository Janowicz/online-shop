package com.example.shop.utility;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class PolConstants {

    public static final String POL = "POL";

    public static final Map<String, String> mapOfPolProvinces = new HashMap<String, String>(){
        {
            put("DŚ", "dolnośląskie");
            put("KP", "kujawsko-pomorskie");
            put("LB", "lubelskie");
            put("LS", "lubuskie");
            put("ŁD", "łódzkie");
            put("MP", "małopolskie");
            put("MZ", "mazowieckie");
            put("OP", "opolskie");
            put("PK", "podkarpackie");
            put("PL", "podlaskie");
            put("PM", "pomorskie");
            put("ŚL", "śląskie");
            put("ŚK", "świętokrzyskie");
            put("WM", "warmińsko-mazurskie");
            put("WP", "wielkopolskie");
            put("ZP", "zachodnio-pomorskie");
        }
    };

    public final static List<String> listOfPolProvinceCode = new ArrayList<>(mapOfPolProvinces.keySet());
    public final static List<String> listOfPolProvinceNames = new ArrayList<>(mapOfPolProvinces.values());
}
