package com.example.shop.repository;

import com.example.shop.domain.PictureToCartItem;
import org.springframework.data.repository.CrudRepository;

public interface PictureToCartItemRepository extends CrudRepository<PictureToCartItem, Long> {
}
