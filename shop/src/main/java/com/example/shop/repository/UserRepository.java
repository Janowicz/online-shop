package com.example.shop.repository;

import com.example.shop.domain.User;
import org.springframework.data.repository.CrudRepository;

public interface UserRepository extends CrudRepository<User, Long> {

    User findByUsername(String username);

//    User findByName(String username);

    User findByEmail(String email);

//    Optional<User> findById(Long id);
}
