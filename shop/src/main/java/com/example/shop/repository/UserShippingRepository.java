package com.example.shop.repository;

import com.example.shop.domain.UserShipping;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

public interface UserShippingRepository extends CrudRepository<UserShipping, Long> {

    Optional<UserShipping> findById(Long id);

    void deleteById(Long id);
}
