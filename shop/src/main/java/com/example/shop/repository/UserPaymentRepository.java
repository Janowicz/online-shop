package com.example.shop.repository;

import com.example.shop.domain.UserPayment;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

public interface UserPaymentRepository extends CrudRepository<UserPayment, Long> {

    Optional<UserPayment> findById(Long id);

    void deleteById(Long id);
}
