package com.example.shop.repository;

import com.example.shop.domain.Picture;
import org.springframework.data.repository.CrudRepository;

import java.util.List;
import java.util.Optional;

public interface PictureRepository extends CrudRepository<Picture, Long> {

    Optional<Picture> findById(Long id);

    List<Picture> findByCategory(String category);

    List<Picture> findByTitleContaining(String title);
}
