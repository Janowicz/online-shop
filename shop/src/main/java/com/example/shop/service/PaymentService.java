package com.example.shop.service;

import com.example.shop.domain.Payment;
import com.example.shop.domain.UserPayment;
import org.springframework.stereotype.Service;

@Service
public interface PaymentService {

    public Payment setByUserPayment(UserPayment userPayment, Payment payment);
}
