package com.example.shop.service;

import com.example.shop.domain.*;

import java.util.Optional;

public interface OrderService {

    Order createOrder(ShoppingCart shoppingCart, ShippingAddress shippingAddress, BillingAddress billingAddress, Payment payment, String shippingMethod, User user);

    Optional<Order> findById(Long id);
}
