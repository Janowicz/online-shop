package com.example.shop.service;

import com.example.shop.domain.*;

import java.util.List;

public interface CartItemService {

    List<CartItem> findByShoppingCart(ShoppingCart shoppingCart);

    CartItem updateCartItem(CartItem cartItem);

    CartItem addPictureToCartItem(Picture picture, User user, int qty);

    CartItem save(CartItem cartItem);

    List<CartItem> findByOrder(Order order);
}
