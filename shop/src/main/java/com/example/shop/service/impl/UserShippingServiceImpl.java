package com.example.shop.service.impl;

import com.example.shop.domain.UserShipping;
import com.example.shop.repository.UserShippingRepository;
import com.example.shop.service.UserShippingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class UserShippingServiceImpl implements UserShippingService {

    @Autowired
    UserShippingRepository userShippingRepository;

    @Override
    public Optional<UserShipping> findById(Long id) {
        return userShippingRepository.findById(id);
    }

    @Override
    public void removeById(Long id) {
        userShippingRepository.deleteById(id);
    }
}

