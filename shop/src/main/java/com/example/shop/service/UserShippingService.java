package com.example.shop.service;

import com.example.shop.domain.UserShipping;

import java.util.Optional;

public interface UserShippingService {

    Optional<UserShipping> findById(Long id);

    void removeById(Long id);
}
