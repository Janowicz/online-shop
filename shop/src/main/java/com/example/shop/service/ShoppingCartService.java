package com.example.shop.service;

import com.example.shop.NotFoundException;
import com.example.shop.domain.ShoppingCart;

public interface ShoppingCartService {

    ShoppingCart updateShoppingCart(ShoppingCart shoppingCart) throws NotFoundException;

    void clearShoppingCart(ShoppingCart shoppingCart);
}
