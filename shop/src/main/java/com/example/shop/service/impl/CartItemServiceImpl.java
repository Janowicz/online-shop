package com.example.shop.service.impl;

import com.example.shop.domain.*;
import com.example.shop.repository.CartItemRepository;
import com.example.shop.repository.PictureToCartItemRepository;
import com.example.shop.service.CartItemService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.List;

@Service
public class CartItemServiceImpl implements CartItemService {

    @Autowired
    CartItemRepository cartItemRepository;

    @Autowired
    PictureToCartItemRepository pictureToCartItemRepository;

    @Override
    public List<CartItem> findByShoppingCart(ShoppingCart shoppingCart) {
        return cartItemRepository.findByShoppingCart(shoppingCart);
    }

    @Override
    public CartItem updateCartItem(CartItem cartItem) {

        BigDecimal bigDecimal = new BigDecimal(cartItem.getPicture().getOurPrice()).multiply(new BigDecimal(cartItem.getQty()));

        bigDecimal = bigDecimal.setScale(2, BigDecimal.ROUND_HALF_UP);
        cartItem.setSubtotal(bigDecimal);

        cartItemRepository.save(cartItem);

        return cartItem;
    }

    @Override
    public CartItem addPictureToCartItem(Picture picture, User user, int qty) {

        List<CartItem> cartItemList = findByShoppingCart(user.getShoppingCart());

        for (CartItem cartItem : cartItemList) {
            if (picture.getId().equals(cartItem.getPicture().getId())){
                cartItem.setQty(cartItem.getQty() + qty);
                cartItem.setSubtotal(BigDecimal.valueOf(picture.getOurPrice()).multiply(new BigDecimal(qty)));
                cartItemRepository.save(cartItem);
                return cartItem;
            }
        }

        CartItem cartItem = new CartItem();
        cartItem.setShoppingCart(user.getShoppingCart());
        cartItem.setPicture(picture);

        cartItem.setQty(qty);
        cartItem.setSubtotal(BigDecimal.valueOf(picture.getOurPrice()).multiply(new BigDecimal(qty)));
        cartItemRepository.save(cartItem);

        PictureToCartItem pictureToCartItem = new PictureToCartItem();
        pictureToCartItem.setPicture(picture);
        pictureToCartItem.setCartItem(cartItem);
        pictureToCartItemRepository.save(pictureToCartItem);

        return cartItem;
    }

    @Override
    public CartItem save(CartItem cartItem) {

        return cartItemRepository.save(cartItem);
    }

    @Override
    public List<CartItem> findByOrder(Order order) {

        return cartItemRepository.findByOrder(order);
    }


}
