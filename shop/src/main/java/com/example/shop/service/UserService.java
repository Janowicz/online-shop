package com.example.shop.service;

import com.example.shop.domain.User;
import com.example.shop.domain.UserBilling;
import com.example.shop.domain.UserPayment;
import com.example.shop.domain.UserShipping;
import com.example.shop.domain.security.PasswordResetToken;
import com.example.shop.domain.security.UserRole;

import java.util.Optional;
import java.util.Set;

public interface UserService {

    PasswordResetToken getPasswordResetToken(final String token);

    void createPasswordResetTokenForUser(final User user, final String token);

    User findByUsername(String username);

    User findByEmail(String email);

    User createUser(User user, Set<UserRole> userRoles) throws Exception;

    User save(User user);

    void updateUserBilling(UserBilling userBilling, UserPayment userPayment, User user);

    void updateUserShipping(UserShipping userShipping, User user);

    void setUserDefaultPayment(Long userPaymentId, User user);

    void setUserDefaultShipping(Long userShippingId, User user);

    Optional<User> findById(Long id);
}
