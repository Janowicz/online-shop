package com.example.shop.service;

import com.example.shop.domain.ShippingAddress;
import com.example.shop.domain.UserShipping;

public interface ShippingAddressService {

    ShippingAddress setByUserShipping(UserShipping userShipping, ShippingAddress shippingAddress);
}
