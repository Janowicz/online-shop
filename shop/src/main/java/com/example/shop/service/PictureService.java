package com.example.shop.service;

import com.example.shop.domain.Picture;

import java.util.List;
import java.util.Optional;

public interface PictureService {

    List<Picture> findAll();

    Optional<Picture> findOne(Long id);

    List<Picture> findByCategory(String category);

    List<Picture> searchByTitle(String title);
}
