package com.example.shop.service.impl;

import com.example.shop.domain.Picture;
import com.example.shop.repository.PictureRepository;
import com.example.shop.service.PictureService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class PictureServiceImpl implements PictureService {

    @Autowired
    private PictureRepository pictureRepository;

    @Override
    public List<Picture> findAll() {

        List<Picture> pictureList = (List<Picture>) pictureRepository.findAll();
        List<Picture> activePictureList = new ArrayList<>();

        for (Picture picture : pictureList) {
            if (picture.isActive()){
                activePictureList.add(picture);
            }
        }

        return activePictureList;
    }

    @Override
    public Optional<Picture> findOne(Long id) {
        return pictureRepository.findById(id);
    }

    @Override
    public List<Picture> findByCategory(String category) {

        List<Picture> pictureList = pictureRepository.findByCategory(category);
        List<Picture> activePictureList = new ArrayList<>();

        for (Picture picture : pictureList) {
            if (picture.isActive()){
                activePictureList.add(picture);
            }
        }

        return activePictureList;
    }

    @Override
    public List<Picture> searchByTitle(String title) {

        List<Picture> pictureList = pictureRepository.findByTitleContaining(title);

        List<Picture> activePictureList = new ArrayList<>();

        for (Picture picture : pictureList) {
            if (picture.isActive()){
                activePictureList.add(picture);
            }
        }

        return activePictureList;
    }
}
