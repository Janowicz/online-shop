package com.example.shop.service;

import com.example.shop.domain.BillingAddress;
import com.example.shop.domain.UserBilling;

public interface BillingAddressService {

    BillingAddress setByUserBilling(UserBilling userBilling, BillingAddress billingAddress);
}
