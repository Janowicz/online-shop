package com.example.shop.controller;

import com.example.shop.NotFoundException;
import com.example.shop.domain.CartItem;
import com.example.shop.domain.Picture;
import com.example.shop.domain.ShoppingCart;
import com.example.shop.domain.User;
import com.example.shop.service.CartItemService;
import com.example.shop.service.PictureService;
import com.example.shop.service.ShoppingCartService;
import com.example.shop.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

import java.security.Principal;
import java.util.List;
import java.util.Optional;

@Controller
@RequestMapping("/shoppingCart")
public class ShoppingCartController {

    @Autowired
    private UserService userService;

    @Autowired
    private CartItemService cartItemService;

    @Autowired
    private ShoppingCartService shoppingCartService;

    @Autowired
    private PictureService pictureService;

    @RequestMapping("/cart")
    public String shoppingCart(Model model, Principal principal) throws NotFoundException {

        User user = userService.findByUsername(principal.getName());
        ShoppingCart shoppingCart = user.getShoppingCart();

        List<CartItem> cartItemList = cartItemService.findByShoppingCart(shoppingCart);
        shoppingCartService.updateShoppingCart(shoppingCart);

        model.addAttribute("cartItemList", cartItemList);
        model.addAttribute("shoppingCart", shoppingCart);

        return "shoppingCart";
    }

    @RequestMapping("/addItem")
    public String addItem(
            @ModelAttribute("picture")Picture picture,
            @ModelAttribute("qty")String qty,
            Model model,
            Principal principal) {

        User user = userService.findByUsername(principal.getName());
        picture = pictureService.findOne(picture.getId()).get();

        if (Integer.parseInt(qty) > picture.getInStockNumber()){
            model.addAttribute("notEnoughtStock", true);

            return "forward:/pictureDetail?id=" + picture.getId();
        }


        cartItemService.addPictureToCartItem(picture, user, Integer.parseInt(qty));
        model.addAttribute("addBookSuccess", true);

        return "forward:/pictureDetail?id=" + picture.getId();
    }

}
