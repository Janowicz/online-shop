package com.example.shop.controller;

import com.example.shop.domain.Picture;
import com.example.shop.domain.User;
import com.example.shop.service.PictureService;
import com.example.shop.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.security.Principal;
import java.util.List;

@Controller
public class SearchController {

    @Autowired
    private UserService userService;

    @Autowired
    private PictureService pictureService;

    @RequestMapping("/searchByCategory")
    public String searchByCategory(
            @RequestParam("category") String category,
            Model model,
            Principal principal){

        if (principal != null){
            String username = principal.getName();
            User user = userService.findByUsername(username);
            model.addAttribute("user", user);
        }

        String classActiveCategory = "active" + category;
        classActiveCategory = classActiveCategory.replaceAll("\\s+", "");
        classActiveCategory = classActiveCategory.replaceAll("&", "");
        model.addAttribute(classActiveCategory, true);

        List<Picture> pictureList = pictureService.findByCategory(category);

        if (pictureList.isEmpty()){
            model.addAttribute("emptyList", true);
            return "catalog";
        }

        model.addAttribute("pictureList", pictureList);

        return "catalog";
    }

    @RequestMapping("/searchPicture")
    public String searchPicture(
            @ModelAttribute("keyword") String keyword,
            Principal principal,
            Model model){

        if (principal != null){
            String username = principal.getName();
            User user = userService.findByUsername(username);
            model.addAttribute("user", user);
        }

        List<Picture> pictureList = pictureService.searchByTitle(keyword);

        if (pictureList.isEmpty()){
            model.addAttribute("emptyList", true);
            return "catalog";
        }

        model.addAttribute("pictureList", pictureList);

        return "catalog";
    }
}
