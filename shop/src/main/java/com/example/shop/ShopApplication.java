package com.example.shop;

import com.example.shop.domain.User;
import com.example.shop.domain.security.Role;
import com.example.shop.domain.security.UserRole;
import com.example.shop.service.UserService;
import com.example.shop.utility.SecurityUtility;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.HashSet;
import java.util.Set;

@SpringBootApplication
public class ShopApplication implements CommandLineRunner {

    @Autowired
    private UserService userService;

    public static void main(String[] args) {
        SpringApplication.run(ShopApplication.class, args);
    }

    @Override
    public void run(String... args) throws Exception {
        User user1 = new User();
        user1.setFirstname("Mateusz");
        user1.setLastname("Janowicz");
        user1.setUsername("m");
        user1.setPassword(SecurityUtility.passwordEncoder().encode("p"));
        user1.setEmail("m.janowicz1993@wp.pl");
        Set<UserRole> userRoles = new HashSet<>();
        Role role1 = new Role();
        role1.setRoleId(1);
        role1.setName("ROLE_USER");
        userRoles.add(new UserRole(user1, role1));

        userService.createUser(user1, userRoles);

    }
}
