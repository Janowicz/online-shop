The project was divided into two models to manage separately:
1. Adminportal
2. Online shop

1. Adminportal is used for administration (add and manage products).
2. Online shop is website for customers. It's enable register, login and manage profile, contains shopping cart and manage products, e-mail confirmation. 

AdminPortal url: localhost:8283
CustomerPortal url: localhost:8282
MySQL Database: stringveryart_db (automaticly create)

Default Admin account: username:admin, password:admin
Default Customer account: username:m, password:p

For correctly load pictures need to change path:
Adminportal:
- com\example\adminportal\controller\PictureController.java

Used technologies: Spring Framework, Thymeleaf, javax.mail, MySQL, HTML, CSS, Hibernate, JavaScript